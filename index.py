"""
------------------------------------------------------------------------
                        EXPERIMENTING...
------------------------------------------------------------------------
"""


# This is where we import the modules.
import requests


def configure():
        """
        ---------------------------------
        Here is what each variable does: 
        ---------------------------------

        ----------------------------------------------------
        1. This is an identifier that allows Reddit to give
        you to access their API by registering the 
        application

        2. It is a special code that allows Reddit to give 
        you to access their API by putting the secret
        code, which the platform gives when you register
        the app 

        3. This is a generated web address that Reddit gives 
        to you when you register the app to Reddit. 

        4  This gets Reddit to know that kind of app "browser"
        that is using to make the type of request to the 
        Reddit API. 


        5. This is where you provide the user name of your
        reddit account. 

        6. This is where you provide the password of your reddit account
        ----------------------------------------------------
        """

        CLIENT_ID = "your_client_id"  # This is where you specify the CLIENT ID
        CLIENT_SECRET = "your_client_secret"  # This is where you specify the SECRET ID
        USERNAME = "your_username"  # This is where you specify the USERNAME
        PASSWORD = "your password"  # This is where you specify the PASSWORD


configure()


def posting():
        """ 
        This is where you are able to configure the ability to post to reddit 
        """


        POST_ENDPOINT = "https://oauth.reddit.com/api/v1/me"

        SUB_REDDIT    = "subreddit name here"

        POST_TITLE    = "post title here"

        POST_BODY     = "post body content here"

        POST_HEADERS  = {"User-Agent": "your_user_agent", 
                        "Authorization": f"bearer {CLIENT_SECRET}",
                        "Content-Type": "application/json"}


        POST_DATA     = {"title": POST_TITLE, 
                         "text": POST_BODY, 
                         "sr": SUBREDDIT}



        POST_RESPONSE = requests.post(f"{POST_ENDPOINT}/submit", 
                                      POST_HEADERS=POST_HEADERS, 
                                      data=json.dumps(data), 
                                      auth=requests.auth.HTTPBasicAuth(USERNAME, PASSWORD))


posting()